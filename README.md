# Experimental reimplementation of the Git commit hash
This is demo code. It creates the Git commit hash of a single file.

See [this](https://stackoverflow.com/questions/35430584/how-is-the-git-hash-calculated) for explanations.

Start the program with `cargo run`.

Run tests that compare the calculated commit hash with Git's commit hash using `cargo test`.
