use std::{env, fs, process::Command, collections::HashMap};

use crate::{get_commit_hash, simple_commit};

#[test]
/// Creates a Git repo in a temporary directory, adds a file, gets the commit hash from Git, and
/// compares that hash to the commit hash we calculate in main.rs.
fn calculated_hash_matches_git_hash() {
    use std::process::Command;

    let cur_test_dir = get_test_dir();

    println!("Current test dir: {cur_test_dir}");

    assert!(fs::create_dir_all(cur_test_dir.clone()).is_ok());
    assert!(env::set_current_dir(&cur_test_dir).is_ok());

    assert!(Command::new("git").arg("init").output().is_ok());

    let file_name = "your_file";
    let file_content = b"File content\n";
    create_test_file(file_name, file_content);

    assert!(Command::new("git").args(["add", "-A"]).output().is_ok());

    let name = "Firstname Lastname";
    let email = "test@example.com";

    let commit_msg = "Message of first commit";

    let date_string = "Wed Jun 23 18:02:18 2021 CEST";
    let commit_hash_from_git = get_commit_hash_from_git(name, email, date_string, commit_msg);

    let commit_date_timestamp = get_commit_date_timestamp(date_string);

    let author_name_and_email = &format!("{name} <{email}>");
    // +0200 is for time zone CEST
    let author_timestamp_and_timezone = &format!("{commit_date_timestamp} +0200");
    let commit = simple_commit(
        author_name_and_email,
        author_timestamp_and_timezone,
        commit_msg,
        // No parent commit hash since this is the first commit
        None,
    );

    let calculated_commit_hash = get_commit_hash(file_name, file_content, &commit);
    println!("Calculated commit hash: {calculated_commit_hash}");
    assert_eq!(commit_hash_from_git, calculated_commit_hash);
}

fn get_commit_date_timestamp(date_string: &str) -> String {
    let date_output = Command::new("date")
        .arg(format!("--date={date_string}"))
        .arg("+%s")
        .output()
        .expect("Failed to call date");

    assert!(
        !date_output.stdout.is_empty(),
        "Standard out of date output shouldn't be empty. Output: {date_output:?}"
    );
    to_utf8_without_newline(&date_output)
}

fn to_utf8_without_newline(output: &std::process::Output) -> String {
    let utf_with_newline = String::from_utf8_lossy(&output.stdout);
    strip_trailing_newline(&utf_with_newline).to_string()
}

fn get_commit_hash_from_git(name: &str, email: &str, date: &str, commit_msg: &str) -> String {
    let git_env_vars = HashMap::from([
        ("GIT_AUTHOR_NAME", name),
        ("GIT_COMMITTER_NAME", name),
        ("GIT_AUTHOR_EMAIL", email),
        ("GIT_COMMITTER_EMAIL", email),
        ("GIT_AUTHOR_DATE", date),
        ("GIT_COMMITTER_DATE", date),
    ]);

    let _git_commit_output = Command::new("git")
        .arg("commit")
        .arg("-am")
        .arg(commit_msg)
        .envs(&git_env_vars)
        .output()
        .expect("Failed to call \"git commit\"");

    // Gets the commit hash according to Git
    let git_rev_parse = Command::new("git")
        .args(["rev-parse", "HEAD"])
        .output()
        .expect("Failed to call \"git rev-parse HEAD\"");

    to_utf8_without_newline(&git_rev_parse)
}

fn create_test_file(file_name: &str, file_content: &[u8]) {
    use std::fs::File;
    use std::io::prelude::*;
    let mut file = File::create(file_name).unwrap();
    assert!(file.write_all(file_content).is_ok());
}

fn strip_trailing_newline(input: &str) -> &str {
    input
        .strip_suffix("\r\n")
        .or(input.strip_suffix('\n'))
        .unwrap_or(input)
}

fn get_test_dir() -> String {
    use std::time::SystemTime;
    let tmp_dir = std::env::temp_dir();
    let tmp_dir_display = tmp_dir.display();

    let test_dir_base = format!("{tmp_dir_display}/calculate_git_commit_hash_tests");


    let timestamp = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_micros();
    format!("{test_dir_base}/{timestamp}")
}
