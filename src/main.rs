use std::{fs, io};

#[cfg(test)]
mod tests;

fn main() -> io::Result<()> {
    let file_name = "your_file";
    let file_content = fs::read(file_name)?;

    let first_commit = simple_commit(
        "Firstname Lastname <test@example.com>",
        // Timestamp calculated using: date --date='Wed Jun 23 18:02:18 2021' +"%s"
        "1624464138 +0200",
        "Message of first commit",
        // No parent commit hash since this is the first commit
        None,
    );

    let first_commit_hash = get_commit_hash(file_name, &file_content, &first_commit);
    Ok(println!("Git commit hash: {first_commit_hash}"))
}

pub fn get_commit_hash(file_name: &str, file_content: &[u8], commit: &CommitMetaData) -> String {
    let file_object_id = git_hash_object(file_content);
    let object_entry = object_entry(file_name, &file_object_id);
    let tree_object_hash = tree_object_hash(&object_entry);

    let commit_hash = commit_hash(commit, &tree_object_hash);
    to_hex_str(&commit_hash)
}

pub fn simple_commit<'a>(
    author_name_and_email: &'a str,
    author_timestamp_and_timezone: &'a str,
    commit_message: &'a str,
    parent_commit_hash: Option<&'a str>,
) -> CommitMetaData<'a> {
    CommitMetaData {
        author_name_and_email,
        author_timestamp_and_timezone,
        committer_name_and_email: author_name_and_email,
        committer_timestamp_and_timezone: author_timestamp_and_timezone,
        commit_message,
        parent_commit_hash,
    }
}

#[derive(Debug)]
pub struct CommitMetaData<'a> {
    pub(crate) author_name_and_email: &'a str,
    pub(crate) author_timestamp_and_timezone: &'a str,
    pub(crate) committer_name_and_email: &'a str,
    pub(crate) committer_timestamp_and_timezone: &'a str,
    pub(crate) commit_message: &'a str,
    // All commits after the first one have a parent commit
    pub(crate) parent_commit_hash: Option<&'a str>,
}

fn commit_hash(commit: &CommitMetaData, tree_object_hash: &str) -> Vec<u8> {
    let author_line = format!(
        "{} {}",
        commit.author_name_and_email, commit.author_timestamp_and_timezone
    );
    let committer_line = format!(
        "{} {}",
        commit.committer_name_and_email, commit.committer_timestamp_and_timezone
    );

    // If it's the first commit, which has no parent,
    // the line starting with "parent" is omitted
    let parent_commit_line = match commit.parent_commit_hash {
        Some(parent_commit_hash) => format!("\nparent {parent_commit_hash}"),
        None => String::new(),
    };
    let commit_msg = commit.commit_message;
    let git_cat_file_str = format!(
        "tree {tree_object_hash}\
        {parent_commit_line}\n\
        author {author_line}\n\
        committer {committer_line}\n\n\
        {commit_msg}\n"
    );

    let git_cat_file_len = git_cat_file_str.len().to_string();

    let commit_object = [
        "commit ".as_bytes(),
        git_cat_file_len.as_bytes(),
        b"\0",
        git_cat_file_str.as_bytes(),
    ]
    .concat();

    // Return the Git commit hash
    to_sha1(&commit_object)
}

/// Converts bytes to their hexadecimal representation.
fn to_hex_str(bytes: &[u8]) -> String {
    bytes.iter().map(|byte| format!("{byte:02x}")).collect()
}

fn tree_object_hash(object_entry: &[u8]) -> String {
    let object_entry_size = object_entry.len().to_string();

    let tree_object = [
        "tree ".as_bytes(),
        object_entry_size.as_bytes(),
        b"\0",
        object_entry,
    ]
    .concat();

    to_hex_str(&to_sha1(&tree_object))
}

fn object_entry(file_name: &str, object_id: &[u8]) -> Vec<u8> {
    // It's a regular, non-executable file
    let mode = "100644";

    // [mode] [file name]\0[object ID]
    let object_entry = [
        mode.as_bytes(),
        b" ",
        file_name.as_bytes(),
        b"\0",
        object_id,
    ]
    .concat();

    object_entry
}

fn to_sha1(hash_me: &[u8]) -> Vec<u8> {
    use sha1::{Digest, Sha1};

    let mut hasher = Sha1::new();
    hasher.update(hash_me);
    hasher.finalize().to_vec()
}

/// Get the object ID
fn git_hash_object(file_content: &[u8]) -> Vec<u8> {
    let file_size = file_content.len().to_string();
    let hash_input = [
        "blob ".as_bytes(),
        file_size.as_bytes(),
        b"\0",
        file_content,
    ]
    .concat();
    to_sha1(&hash_input)
}
